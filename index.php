<?php
// ini_set('display_errors', '1');
// error_reporting(E_ALL);

include 'Rss.php';

$feeds = new Rss();
$news = $feeds->getNews();

if($feeds->getNewsCount() == 0):
	$feeds->getRssFeeds();
endif;

$topNews = $feeds->getTopNews();

$id = $_GET['news_id'];
$newsItem = $feeds->getNewsItem($id);

$feeds->updateViews($id);

$views = $feeds->getNewsViews($id);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Rss News Reader</title>
	<script type="text/javascript" src="js/main.js"></script>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<style type="text/css">
		div{
			border-bottom:1px solid #f4f4f4;
		}
		.col-md-7, .col-md-5{
			border-right:1px solid #f4f4f4;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<div class="header">
					<h3>Rss Feeds</h3>
					<p>Click on a news item on the side bar to view what's it about</p>
				</div>
				<h3><?php echo $newsItem->newsTitle; ?></h3>
				<p><?php echo $newsItem->newsDescription; ?></p>
				<?php if($newsItem->newsTitle): ?>
					views <span class="badge"><?php echo $views; ?></span>
				<?php endif; ?>
				<br />
			</div>
			<div class="col-md-5">
			<h3>Top News</h3>
				<ul>
					<?php foreach($topNews as $key => $value): ?>
						<li>
							<a href="?news_id=<?php echo $value['id']; ?>	"><?php echo $value['newsTitle']; ?></a>
						</li>
					<?php endforeach; ?>
				</ul>

			<hr />	
			
			`<h3>Other News</h3>
				<ul>
					<?php foreach($news as $key => $value): ?>
						<li>
							<a href="?news_id=<?php echo $value['id']; ?>	"><?php echo $value['newsTitle']; ?></a>
						</li>
					<?php endforeach; ?>
				</ul>
				<label clas
			</div>
		</div>
	</div>
</body>
</html>
