# RSS News Reader #
Pulls rss feeds from nation.co.ke/rss and orders top stories based on most viewed content.

**Requirements**

* php 5 or greater
* mysql

**Set-up steps**

1. clone the repo onto your machine
1. cd to rssreader
1. start the php server 
```
#!python

php -S localhost:8000
```