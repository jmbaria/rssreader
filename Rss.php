<?php

class Rss
{
	private $host = 'localhost';
	private $dbname = 'newsfeeds';
	private $username = 'root';
	private $password = 'Hg:*Fl9|-yQPceZ8Wwh0';
	private $rssFeed = 'http://www.nation.co.ke/rss';
	private $rss = 'nation';
	private $limit = 10;

    /**
     * Initialise
     */
    public function __construct()
    {
    	$this->getNews();
    }

    /**
     * Connecto to a data store
     * @return Object Connection object
     */
    public function connectDB(){
    	$dbh = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->username, $this->password);
    	return $dbh;
    }

    /**
     * Pull rss feeds from news feeder
     * @return null
     */
    public function getRssFeeds()
    {
        $rss     = file_get_contents($this->rssFeed);
        $xmlFeed = new SimpleXmlElement($rss);
        foreach ($xmlFeed as $key => $feed):
            $this->addNewsFeed($this->rss, (string) $feed->title, (string) $feed->description, (string) $feed->link);
        endforeach;
    }

    /**
     * Insert feeds into database
     * @param String $source Feed source
     * @param String $title  News title
     * @param String $link   News feed link
     */
    private function addNewsFeed($source, $title, $description, $link){
    	$dbh = $this->connectDB();
    	$query = "insert into news values( null, :source, :title, :description, :link, 0, now(), 0)";
    	$stmt = $dbh->prepare($query);
    	$stmt->bindParam(':source', $source, PDO::PARAM_STR);
    	$stmt->bindParam(':title', $title, PDO::PARAM_STR);
    	$stmt->bindParam(':description', $description, PDO::PARAM_STR);
    	$stmt->bindParam(':link', $link, PDO::PARAM_STR);
    	$response = $stmt->execute();

    	return $response;
    }

    public function getNews(){
    	$dbh = $this->connectDB();
    	$query = "select * from news limit :limit ";
    	$stmt = $dbh->prepare($query);
    	$stmt->bindParam(':limit', $this->limit, PDO::PARAM_INT);
    	$stmt->execute();
    	$news = $stmt->fetchAll();

    	return $news;
    }

    public function getNewsItem($id){
    	$dbh = $this->connectDB();
    	$query = "select * from news where id = :itemId";
    	$stmt = $dbh->prepare($query);
    	$stmt->bindParam(':itemId', $id, PDO::PARAM_INT);
    	$stmt->execute();
    	$news = (object) $stmt->fetch();

    	return $news;
    }

    /**
     * Get no of times the news item has been viewed
     * @param  Int $id newsId
     * @return Int     no.of views
     */
    public function getNewsViews($id){
    	$dbh = $this->connectDB();
    	$stmt = $dbh->prepare("select newsViews from news where id = :id");
    	$stmt->bindParam(':id', $id, PDO::PARAM_INT);
    	$stmt->execute();
    	$views = (object) $stmt->fetch();

    	return $views->newsViews;
    }


    /**
     * Update news views
     * @param  Int $id Newsid
     * @return void
     */
    public function updateViews($id){
    	$dbh = $this->connectDB();
    	$count = $this->getNewsViews($id)+1;

    	$query = "update news set newsViews = :count where id = :id";
    	$stmt = $dbh->prepare($query);
    	$stmt->bindParam(':count', $count, PDO::PARAM_INT);
    	$stmt->bindParam(':id', $id, PDO::PARAM_INT);
    	$response = $stmt->execute();
    }

    public function getTopNews(){
    	$dbh = $this->connectDB();

    	$query = "select * from news where newsViews > 0 order by newsViews desc limit 5";
    	$stmt = $dbh->prepare($query);
    	$stmt->execute();
    	$topNews = $stmt->fetchAll();

    	return $topNews;
    }


    public function getNewsCount(){
    	$dbh = $this->connectDB();

    	$query = "select count(*) as count from news";
    	$stmt = $dbh->query($query);
    	$stmt->execute();
    	$newsCount = (object) $stmt->fetch();
    	
	 	return $newsCount->count;
    }

}
